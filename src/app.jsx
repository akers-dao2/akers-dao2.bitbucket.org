import React from 'react';
var ReactDom = require('react-dom');
import {Header} from './header.jsx';
import {SideBar} from './sideBar.jsx';
import {MainContent} from './mainContent.jsx';

/**
*Main component implementing all components
*@author Falos Akers
*@extends {React.Component}
*/

class ResumeApp extends React.Component{


	constructor() {
		super();
	}


	render() {
		return (
			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header mdl-layout--fixed-drawer">
				<Header />
				<SideBar />
				<main className="mdl-layout__content">

					<div className="mdl-grid">
						<div className="mdl-cell mdl-cell--1-col">
							
						</div>
						<div className="mdl-cell mdl-cell--11-col">
							<MainContent className="animated slideInDown"/>
						</div>
						
					</div>
				
 				 </main>
			</div>
		)
	}
}

ReactDom.render(<ResumeApp/>, document.getElementById('container'));