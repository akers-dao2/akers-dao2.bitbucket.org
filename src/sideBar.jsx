import React from 'react';
/**
*Side Bar component
*@author Falos Akers
*@extends {React.Component}
*/

export class SideBar extends React.Component{
	constructor(){
		super();
		/**
		* List of skills for side bar.
		* @type {Array.<{text:string,id:string,value:number}>}
		*/
		this.skills = [
			{text:'JavaScript',id:'JavaScript',value:90},
			{text:'Angularjs',id:'Angularjs',value:90},
			{text:'Reactjs',id:'react',value:50},
			{text:'Nodejs',id:'Nodejs',value:50},
			{text:'C#',id:'csharp',value:50},
			{text:'Objective C',id:'objectC',value:50},
			{text:'HTML',id:'html',value:100},
			{text:'CSS3',id:'css',value:60},
			{text:'Powershell',id:'Powershell',value:50},
		];
	}
	
	/**
	*Create array of HTMLLIELEMENT for knowledge side bar list
	*@return {Array.<HTMLLIELEMENT>} Array of side bar knowledge list items
	*/
	_renderKnowledgeList(){
		/**
		* List of knowledges for side bar.
		* @type {Array.<string>}
		*/
		let knowledges = ['Mobile / Web Development','Automation','SQL Server & NoSQL', 'Object-Orient Programming'];
		
		/**
		* List of knowledges for side bar.
		* @type {Array.<HTMLLIELEMENT>}
		*/
		let _knowledges = knowledges.map((knowledge,index)=>{
			return (
				<li key={index} className="side-bar-list animated fadeInLeft" data-index={index} id={"data-"+index}>
					<i className="material-icons side-bar-icons">arrow_forward</i>
					{knowledge}
				</li>
			)
		})
		
		return _knowledges;
		
	}
	
	/**
	*Create array of HTMLLIELEMENT for skill side bar list
	*@return {Array.<HTMLLIELEMENT>} Array of side bar knowledge list items
	*/
	_renderSkillsList(){
		/**
		* List of skills for side bar.
		* @type {Array.<HTMLLIELEMENT>}
		*/
		let _skills = this.skills.map((skill,index)=>{
			return (
				<li key={index} className="side-bar-list">
					<i className="material-icons side-bar-icons">arrow_forward</i>
					{skill.text}
					<div ref="skillGraph" id={skill.id} className="mdl-progress mdl-js-progress side-bar-progress"></div>
				</li>
			)
		})
		
		return _skills;
		
	}
	
	/**
	*Create title header for side bar list
	*@return {HTMLDIVELEMENT} side bar header DIV element
	*/
	_renderTitleHeader(){
		return (
			<div className="side-bar-header-container">
				<span className="side-bar-header"><i className="material-icons animated bounce" id="work-icon">work</i>Developer's Briefcase</span>
			</div>
		)
	}
	
	/**
	*When component mounts set progress bars for each skill list item
	*/
	
	componentDidMount(){
		this.skills.map((skill,index)=>{
			document.querySelector('#'+skill.id).addEventListener('mdl-componentupgraded', function() {
				var index = 0;
				setInterval(()=>{
					this.MaterialProgress.setProgress(skill.value);
					index
				},2000)
				
			});
		});

	}
	
	render(){
		return (
			<div className={"mdl-layout__drawer side-bar-container "+this.props.className}>
				<span>{this._renderTitleHeader()}</span>
				<div className="side-bar-title">Knowledge</div>
				<div>
					{this._renderKnowledgeList()}
				</div>
				<div className="side-bar-title">Skills</div>
				<div>
					{this._renderSkillsList()}
				</div>
				<div className="side-bar-title">Web</div>
				<div>
					<div className="side-bar-list"><i className="material-icons icon">email</i><a href="mailto:Akers.dao2@gmail.com" className="side-bar-email">akers.dao2@gmail.com</a></div>
					<div className="side-bar-list"><i className="material-icons icon">web</i><a href="https://bitbucket.org/akers-dao2/" className="side-bar-email">https://bitbucket.org/akers-dao2/</a></div>
				</div>
			</div>
		)
	}
}