import React from 'react';

/**
*Header component
*@author Falos Akers
*@extends {React.Component}
*/
export class Header extends React.Component {
	constructor() {
		super();
	}
	render(){
		return (
			<header className={"mdl-layout__header header-head " +this.props.className}>
				<div className="mdl-layout__header-row">	
					<div>
						<div className="mdl-layout-title ">Falos Akers</div>			
						<div className="header-title">Web Developer</div>			
						<div className="header-address">643 Farmington Ave Kensington, CT 06037</div>			
					</div>
				</div>
				
			</header>

				
		);
	}
	
	
}