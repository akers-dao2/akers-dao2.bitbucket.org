import React from 'react';
/**
*Main content section component
*@author Falos Akers
*@extends {React.Component}
*/

export class MainContent extends React.Component {
	constructor(){
		super();
	}
	
	/**
	*
	*@return {Array<HTMLDIVELEMENT>} Returns a list of array elements
	*/
	
	_cards(){
		/**
		* card data.
		* @type {Array.<{exp:string,icon:string,data:Array.<{text:string,url:string,supportText:string}>}>}
		*/
		let cards = [
			{exp:'Projects',icon:"computer",data: [
					{text:'Wells Reports',url:"wells-reports-resume",supportText:'Application for generating Chimney Inspection Reports'},
					{text:'Online Portfolio',url:"akers-dao2.bitbucket.org",supportText:'Online Portfolio representing my business work.'},
					{text:'FFC Financial App',url:"ffc-financial-app",supportText:'An application that facilitated the management of tithes and offerings for a local church.'},
					{text:'Installation Matrix',url:"installation-matrix",supportText:'An application that facilitates quoting implementation projects making quoting projects both easy, efficient and consistent for all projects.'},
					{text:'Settings Utility',url:"settings-utility",supportText:'An application that helped create SQL scripts for easy migration of online banking clients from one platform to another one.'},
					{text:'Implementations Routing',url:"implementations-routing",supportText:'An application that created routing within the web.config file used for managing traffic for mobile banking applications.'},
				]},
			{exp:'8 Years Experience',icon:"done_all",data: [{id:'', text:'Implementation Tools Developer, Fiserv.',supportText:'Glastonbury, CT — 2004-Current'},{id:'', text:'Treasury/Board Member - Faith Family Church',supportText:'Glastonbury, CT — 2006-2014'}]},
			{exp:'Education Value',icon:"school",data: [{id:'', text:'Montgomery County Community College, Pottstown PA',supportText:'Associated Degree of Computer Science — 1998-2001'},{id:'', text:'Montgomery County Community College, Pottstown PA',supportText:'Associated Degree of Business Administration — 1998-2001'}]},
		];
		
		/**
		* creates new array of card HTML elements based on card data
		* @type {Array.<HTMLDIVELEMENT>}
		*/ 
		let listOfCards = cards.map((card,index)=>{
			return (
				<div className="main-content-card-container" key={index}>
					<div className="main-content-heading"><i className="material-icons icon">{card.icon}</i>{card.exp}</div>
					{card.data.map((card,index)=>{
						return (
							<div className="mdl-card mdl-shadow--2dp main-content-card-square"  key={index}>
								<div className="mdl-card__title">
									<span className="mdl-card__title-text cardText main-content-card-heading">{card.text}</span>
								</div>
								<div className="mdl-card__supporting-text detail hide" ref="detail">sdfasdfadsfa</div>
								<div className="mdl-card__supporting-text">
									{card.supportText}
								</div>
								{
									(()=>{
										if(card.url){
											return(	
												<div className="mdl-card__supporting-text">
													<a href={"https://bitbucket.org/akers-dao2/" + card.url} className="main-content-email" target="_blank">https://bitbucket.org/akers-dao2/{card.url}</a>
												</div>
											)	
										}		
									})()
									
								}
							</div>
						)
					})}
				</div>
			)
		})
		
		return listOfCards
	}
	
	//add click handler for card
	componentDidMount(){
		document.querySelector('.cardText').addEventListener('click', (e)=> {
			console.log(e.parentNode);
		});
	}
	
	
	render(){
		return (
				<div className={this.props.className} id="mainContent">
					{this._cards()}
				</div>
			
		)
	}
}