module.exports = {
  entry: './src/app.jsx',
  output: {
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  Default: ["", ".webpack.js", ".web.js", ".js", ".jsx"],
  module: {
    loaders: [
      { 
        test: /\.jsx$/, 
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
        query: {
          presets: ['es2015', 'react'],
          cacheDirectory:true
        }   
    }]
  }
}