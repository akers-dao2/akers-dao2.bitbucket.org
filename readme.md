##Online Portfolio

---------
	
Online Portfolio representing my business work.

**Installation:**

You need node.js and npm. At the root of the project type:

```node
npm install
```

**Run Project:**

```node
npm run play
```

**Software Used:**

1. ReactJs (https://facebook.github.io/react/index.html) use for view model
1. Material Design (http://www.getmdl.io/)
1. Animation (http://daneden.github.io/animate.css/)
1. WebPack (http://webpack.github.io/) workflow tooling

----------
**Example:**

- Displays projects
- Displays top skills
- Displays top knowledge areas


![screenshot](screenshot.png)